
function quantized = quantway(image, quant_factor, title)

   z = [16    11    10    16    24    40    51    61
        12    12    14    19    26    58    60    55
        14    13    16    24    40    57    69    56
        14    17    22    29    51    87    80    62
        18    22    37    56    68   109   103    77
        24    35    55    64    81   104   113    92
        49    64    78    87   103   121   120   101
        72    92    95    98   112   100   103    99] * quant_factor;

   T = dctmtx(8);

   trans = round(blockproc(image - 128, [8 8], @(x) (T*double(x.data)*T')./z));
   trans_without_quant = blockproc(image - 128, [8 8], @(x) T*double(x.data)*T');

   truncated_trans = round(blockproc(trans, [8 8], @(x) x.data.*z));
   inverse_truncated_trans = round(blockproc(truncated_trans, [8 8], @(x) T'*x.data*T)) + 128;

   imshow_psnr(title, uint8(inverse_truncated_trans), uint8(image));
   imwrite(imscale(inverse_truncated_trans), strcat([ ...
      'img/', ...
      title, ...
      '.jpg', ...
   ]));
end
