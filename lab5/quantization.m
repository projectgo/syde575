clc; clear all; close all;

lena = int16(rgb2gray(imread('lena.tiff')));

% imshow_title(lena, 'original lena image');

quantway(lena, 1,  '20_Z');
quantway(lena, 3,  '21_3Z');
quantway(lena, 5,  '22_5Z');
quantway(lena, 10, '23_10Z');
