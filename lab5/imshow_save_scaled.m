function imshow_save_scaled(image, name)
   figure;
   image_min = min(min(image));
   image_max = max(max(image));
   image_range = image_max - image_min;
   image = (image-image_min)./image_range;
   imshow(image);
   colormap(jet);
   imwrite(image, strcat([ ...
      'img/', ...
      name, ...
      '.jpg', ...
   ]));
end
