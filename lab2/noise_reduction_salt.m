clear;
close all;

pure_lena = im2double(rgb2gray(imread('lena.tif')));
figure;
imshow(pure_lena);
title('pure lena');

figure;
imhist(pure_lena);
title('pure lena');

%%%%%%%%%%%%%%%%%%%%%%%
%%% Salt & Peppered %%%
%%%%%%%%%%%%%%%%%%%%%%%
lena = imnoise(pure_lena, 'salt & pepper', 0.15);

title(imshow_psnr('Salt & peppered lena', lena, pure_lena));
title(imhist_psnr('Salt & peppered lena', lena, pure_lena));
xlim([-0.01 1.01]);

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 7x7 Average Filter %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
avg_filter_7 = fspecial('average', 7);

average7d_lena = imfilter(lena, avg_filter_7);

title(imshow_psnr('7x7 averaged lena', average7d_lena, pure_lena));
title(imhist_psnr('7x7 averaged lena', average7d_lena, pure_lena));

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 7x7 Gaussian Filter %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
gaussian_filter = fspecial('gaussian', 7, 1);

gaussianed_lena = imfilter(lena, gaussian_filter);

title(imshow_psnr('7x7 gaussian blurred lena', gaussianed_lena, pure_lena));
title(imhist_psnr('7x7 gaussian blurred lena', gaussianed_lena, pure_lena));

%%%%%%%%%%%%%%%%%%%%%
%%% Median Filter %%%
%%%%%%%%%%%%%%%%%%%%%
median_lena = medfilt2(lena, [3, 3]);

title(imshow_psnr('3x3 median filtered lena', median_lena, pure_lena));
title(imhist_psnr('3x3 median filtered lena', median_lena, pure_lena));













