function imshow_psnr(image_title, image, reference)
   figure;
   imshow(image, []);
   title( ...
      strcat([ ...
         image_title, ...
         ', psnr = ', ...
         num2str( ...
            psnr(real(reference), real(image)) ...
         ) ...
      ]) ...
   )
end
