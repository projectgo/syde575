function scaled = imscale(image)
   image_min = min(min(image));
   image_max = max(max(image));
   image_range = image_max - image_min;
   scaled = (image-image_min)./image_range;
end
