function psnr = PSNR( reference_image, test_image )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    mse = MSE(reference_image, test_image);
    maximum = double(max(max(reference_image))^2)/double(mse);
    psnr = 10*log10(maximum);
end

