function segment = get_segment(image, segments, segment)
   r = image(:,:,1);
   g = image(:,:,2);
   b = image(:,:,3);
   r(segments ~= segment) = 0;
   g(segments ~= segment) = 0;
   b(segments ~= segment) = 0;
   image(:,:,1) = r;
   image(:,:,2) = g;
   image(:,:,3) = b;
   segment = image;
end
