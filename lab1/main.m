%% 3 Digital Zooming

clc; clear all; close all;

    % Lena.tiff
    lena = imread('lena.tiff');
    graylena = rgb2gray(lena);
    graylenax25 = imresize(graylena, .25);

    glbilinear = imresize(graylenax25, 4, 'bilinear');
    bilinearpsnr = PSNR(graylena, glbilinear);

    glnearest = imresize(graylenax25, 4, 'nearest');
    nearestpsnr = PSNR(graylena, glnearest);

    glbicubic = imresize(graylenax25, 4, 'bicubic');
    bicubicpsnr = PSNR(graylena, glbicubic);

    figure;
    imshow(glbilinear);
    title(strcat('Lena downsampled and 4x digital zoom binlinear transformation PSNR = ', num2str(bilinearpsnr)));

    figure;
    imshow(glnearest);
    title(strcat('Lena downsampled and 4x digital zoom nearest neighbor transformation PSNR = ', num2str(nearestpsnr)));

    figure;
    imshow(glbicubic);
    title(strcat('Lena downsampled and 4x digital zoom bicubic transformation PSNR = ', num2str(bicubicpsnr)));
    
    % cameraman.tif  
    graycameraman = imread('cameraman.tif');
    graycameramanx25 = imresize(graycameraman, .25);

    glbilinear = imresize(graycameramanx25, 4, 'bilinear');
    bilinearpsnr = PSNR(graycameraman, glbilinear);

    glnearest = imresize(graycameramanx25, 4, 'nearest');
    nearestpsnr = PSNR(graycameraman, glnearest);

    glbicubic = imresize(graycameramanx25, 4, 'bicubic');
    bicubicpsnr = PSNR(graycameraman, glbicubic);

    figure;
    imshow(glbilinear);
    title(strcat('Cameraman binlinear transformation PSNR = ', num2str(bilinearpsnr)));

    figure;
    imshow(glnearest);
    title(strcat('Cameraman nearest neighbor transformation PSNR = ', num2str(nearestpsnr)));

    figure;
    imshow(glbicubic);
    title(strcat('Cameraman bicubic transformation PSNR = ', num2str(bicubicpsnr)));
    
%% 4. Discrete Convolution for Image Processing

lena = imread('lena.tiff');
lenagray = rgb2gray(lena);

h1 = (1/6)*ones(1,6);
h2 = h1';
h3 = [-1 1]';

h1lenagray = conv2(h1, lenagray);
h2lenagray = conv2(h2, lenagray);
h3lenagray = conv2(h3, lenagray);

figure;
imshow(h1lenagray, []);
title('h1 Convolution');

figure;
imshow(h2lenagray, []);
title('h2 Convolution');

figure;
imshow(h3lenagray, []);
title('h3 Convolution');

%% 5 Point Operations for Image Enhancement

tire = imread('tire.tif');

figure;
imshow(tire);
title('Grayscale image of tire');

figure;
imhist(tire, 255);
title('Histogram of the intensity levels of tire');

width = length(tire(1,:));
height = length(tire(:,1));
negativetire = uint8(255*ones(height, width)) - tire;

figure;
imshow(negativetire);
title('Negative of tire');

figure;
imhist(negativetire, 255);
title('Histogram of the intensity levels of negative tire');

tire5transform = double(tire).^0.5;
tire13transform = double(tire).^1.3;

figure;
imshow(tire5transform, []);
title('Power law transform of tire (y = 0.5)');

figure;
imshow(tire13transform, []);
title('Power law transform of tire (y = 1.3)');

figure;
maximum = max(max(tire5transform))
imhist(tire5transform./maximum, 255);
title('Histogram of the intensity levels of power law transform of tire (y = 0.5)');

figure;
maximum = max(max(tire13transform))
imhist(tire13transform./maximum, 255);
title('Histogram of the intensity levels of power law transform of tire (y = 1.3)');

figure;
tirehisteq = histeq(tire, 255);
imshow(tirehisteq, []);
title('Histeq on tire image');

figure;
imhist(tirehisteq, 255);
title('Histogram of the intensity levels of histeq of tire');
