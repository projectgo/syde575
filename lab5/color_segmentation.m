clc; clear all; close all;

og_peppers = imread('peppers.png');
% segway(og_peppers, [
%    0 0
%    0 0
% ], '8_K2');
% 
% segway(og_peppers, [
%    55 155
%    200 400
% ], '9_K2_given');
% 
% segway(og_peppers, [
%    0 0
%    0 0
%    0 0
%    0 0
% ], '10_K4');

ab = segway(og_peppers, [
   55 155
   130 110
   200 400
   280 470
], '11_K4_given');

% figure;
% imshow(imscale(ab));

imshow_save(get_segment(og_peppers, ab, 1), '12_segment_1');
imshow_save(get_segment(og_peppers, ab, 2), '13_segment_2');
imshow_save(get_segment(og_peppers, ab, 3), '14_segment_3');
imshow_save(get_segment(og_peppers, ab, 4), '15_segment_4');

