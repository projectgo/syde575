clear all; clc; close all;

lena = int16(rgb2gray(imread('lena.tiff')));

T = dctmtx(8);
imshow_save(T, '16_DCT_Matrix');

trans = floor(blockproc(lena - 128, [8 8], @(x) T*double(x.data)*T'));

% imshow_title(trans, 'Lena with DCT applied')
% imshow_title(lena(1:8, 1:8), 'Lena at 8x8 1, 1')
imshow_save(imscale(trans(1:8, 1:8)), '17_DCT_of_Lena_1_1')
% imshow_title(lena(297:304, 81:88), 'Lena at 8x8 297, 81')
imshow_save(imscale(trans(297:304, 81:88)), '18_DCT_of_Lena_297_81')

mask = [1 1 1 0 0 0 0 0
        1 1 0 0 0 0 0 0
        1 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0];

truncated_trans = floor(blockproc(trans, [8 8], @(x) x.data.*mask));
inverse_truncated_trans = floor(blockproc(truncated_trans, [8 8], @(x) T'*x.data*T)) + 128;

imshow_psnr('Lena compressed by discarding high frequency components', ...
            uint8(inverse_truncated_trans), uint8(lena));
imshow_save(imscale(inverse_truncated_trans), '19_DCT_Lowpassed_Lena')

