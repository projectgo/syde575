% Part 2: Image Restoration in Frequency Domain

clc; clear all; close all;

h = fspecial('disk', 4);
cameraman = im2double(imread('cameraman.tif'));
h_freq = fft2(h, size(cameraman,1), size(cameraman,2)); % pad
cameraman_blurred = real(ifft2(h_freq.*fft2(cameraman)));

imshow(cameraman);
imwrite(cameraman, '0_cameraman_original.jpg');

imshow_psnr('Cameraman Blurred', cameraman_blurred, cameraman);
imwrite(cameraman_blurred, '1_cameraman_blurred_16.8964.jpg');

cameraman_unblurred = real(ifft2(fft2(cameraman_blurred)./h_freq));
imshow_psnr('Cameraman Unblurred', cameraman_unblurred, cameraman);
imwrite(cameraman_unblurred, '2_cameraman_unblurred_253.8354.jpg');

cameraman_blur_noised = imnoise(cameraman_blurred, 'gaussian', 0, 0.002);
imshow_psnr('Cameraman Blurred, Noised', cameraman_blur_noised, cameraman);
imwrite(cameraman_blur_noised, '3_cameraman_blurred_noised_16.4981.jpg');

cameraman_blur_noised_unblurred = real( ...
   ifft2(fft2(cameraman_blur_noised)./h_freq) ...
);
imshow_psnr( ...
   'Cameraman Blurred, Noised, Unblurred', ...
   cameraman_blur_noised_unblurred, ...
   cameraman ...
);
imwrite( ...
   cameraman_blur_noised_unblurred, ...
   '4_cameraman_noised_unblurred_-38.5489.jpg' ...
);

onez = ones(size(cameraman,1), size(cameraman,2));
just_noise = cameraman_blur_noised - cameraman_blurred;
just_noise_freq = fft2(just_noise);
figure;
imshow(fftshift(abs(h_freq)), []);
title('Disc Filter');
figure;
imshow(log(fftshift(abs(onez./h_freq))), []);
title('IDisc Filter');
figure;
imshow(log(fftshift(abs(fft2(cameraman)))), []);
title('Cameraman');
figure;
imshow(log(fftshift(abs(just_noise_freq))), []);
title('Just Noise');
cameraman_blurred_freq = fft2(cameraman_blurred);
figure;
imshow(log(fftshift(abs(cameraman_blurred_freq))), []);
title('Cameraman Blurred');
figure;
nsr = (just_noise_freq./cameraman_blurred_freq).^2;
imshow(log(fftshift(abs(nsr))), []);
title('NSR');
figure;
imshow(log(fftshift(abs((h_freq.^2)./(h_freq.^2 + nsr)./h_freq))), []);
title('WFilter');

cameraman_blur_noised_weiner_unblurred = deconvwnr(cameraman_blur_noised, h, abs(nsr));

imshow_psnr('Cameraman Weinered', cameraman_blur_noised_weiner_unblurred, cameraman);
imwrite(cameraman_blur_noised_weiner_unblurred, '5_cameraman_weinered_16.1778.jpg');
