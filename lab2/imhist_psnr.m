function x = imhist_psnr(title, image, reference)
   figure;
   imhist(image);
   x = strcat([title, ', psnr = ', num2str(psnr(image, reference))])
end
