function x = imshow_psnr(title, image, reference)
   figure;
   imshow(image);
   x = strcat([title, ', psnr = ', num2str(psnr(image, reference))])
end
