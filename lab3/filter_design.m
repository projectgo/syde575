% Part 2: Fourier Analysis
clc; clear all; close all;

frequnoisy = imread('frequnoisy.tif');

% figure;
% imshow(frequnoisy, []);
% title('Noisy image');
imfreq('Noisy image', frequnoisy)

frequnoisy_freq = fft2(frequnoisy, size(frequnoisy, 1), size(frequnoisy, 2));

r = 10;
negative_mask = ones(256, 256);
negative_mask([256/2-r : 256/2+r], [256/2-r : 256/2+r]) = 0;

positive_mask = zeros(256, 256);
positive_mask([256/2-r : 256/2+r], [256/2-r : 256/2+r]) = 1;

center_block = positive_mask .* fftshift(frequnoisy_freq);
outer_block = negative_mask .* fftshift(frequnoisy_freq);
outer_block(outer_block > 1.0481e6) = 0;

max(max(outer_block))

figure;
imshow(log(center_block), []);
title('Inner block');
figure;
imshow(outer_block, []);
title('Outer block');
figure;
imshow(log(outer_block + center_block), []);
title('Fourier Spectra of noised image with specks removed');
figure;
imshow(ifft2(ifftshift(outer_block + center_block)), []);
title('Denoised frequency noised image.');

