function mse = MSE( reference_image, test_image )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    width = length(reference_image(1,:));
    height = length(reference_image(:,1));
    mse = sum(sum(((reference_image - test_image).^2)))./(width*height);
end

