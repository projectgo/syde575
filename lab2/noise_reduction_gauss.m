clear;
close all;

pure_lena = im2double(rgb2gray(imread('lena.tif')));
figure;
imshow(pure_lena);
title('pure lena');

figure;
imhist(pure_lena);
title('pure lena');

%%%%%%%%%%%%%%%
%%% Gaussed %%%
%%%%%%%%%%%%%%%
lena = imnoise(pure_lena, 'gaussian', 0, 0.002);

title(imshow_psnr('Gaussian noised lena', lena, pure_lena));
title(imhist_psnr('Gaussian noised lena', lena, pure_lena));

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  3x3 Average Filter %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
avg_filter = fspecial('average');

averaged_lena = imfilter(lena, avg_filter);

title(filter_show('3x3 average filter', avg_filter));
title(imshow_psnr('3x3 averaged lena', averaged_lena, pure_lena));
title(imhist_psnr('3x3 averaged lena', averaged_lena, pure_lena));

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 7x7 Average Filter %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
avg_filter_7 = fspecial('average', 7);

average7d_lena = imfilter(lena, avg_filter_7);

title(filter_show('7x7 average filter', avg_filter_7));
title(imshow_psnr('7x7 averaged lena', average7d_lena, pure_lena));
title(imhist_psnr('7x7 averaged lena', average7d_lena, pure_lena));

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 7x7 Gaussian Filter %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
gaussian_filter = fspecial('gaussian', 7, 1);

gaussianed_lena = imfilter(lena, gaussian_filter);

title(filter_show('7x7 gaussian blur filter', gaussian_filter));
title(imshow_psnr('7x7 gaussian blurred lena', gaussianed_lena, pure_lena));
title(imhist_psnr('7x7 gaussian blurred lena', gaussianed_lena, pure_lena));


















