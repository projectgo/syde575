function x = filter_show(title, filter)
   figure; imagesc(filter); colormap(gray);
   x = title;
end
