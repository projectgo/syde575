% Part 3: Adaptive Filtering

clc; clear all; close all;

degraded = im2double(imread('degraded.tif'));
cameraman = im2double(imread('cameraman.tif'));

figure;
imshow(degraded);
imwrite(degraded, '6_degraded.jpg');

shirt = degraded(143:187, 50:87);
figure;
imshow(shirt);
imwrite(shirt, '7_shirt.jpg');

noise_variance = var(shirt(:));
local_mean = colfilt(degraded, [5,5], 'sliding', @mean);
local_var = colfilt(degraded, [5,5], 'sliding', @var);
onez = ones(size(degraded,1), size(degraded,2));
Otherwise = onez * noise_variance - local_var;
Otherwise(Otherwise>0) = 0;
Otherwise(Otherwise<0) = 1;

K = (1 - noise_variance./local_var).*Otherwise;
imshow_psnr('Leesified', K .* degraded + (onez-K).*local_mean, cameraman)
imwrite(K .* degraded + (onez-K).*local_mean, '8_leesified_21.6348.jpg');

gaussian_filter= fspecial('gaussian', [size(degraded,1), size(degraded,2)], 30);
gaussian_restored = ifft2(ifftshift(fftshift(fft2(degraded)) .* gaussian_filter));
gaussian_restored = real(gaussian_restored./max(max(gaussian_restored)));
imshow_psnr('Gaussian Restored', gaussian_restored, cameraman)
imwrite(gaussian_restored, '9_gaussed_5.584.jpg');

noise_variance = var(shirt(:))*3;
Otherwise = onez * noise_variance - local_var;
Otherwise(Otherwise>0) = 0;
Otherwise(Otherwise<0) = 1;

K = (1 - noise_variance./local_var).*Otherwise;
imshow_psnr('Leesified, noise variance * 3', K .* degraded + (onez-K).*local_mean, cameraman)
imwrite(K .* degraded + (onez-K).*local_mean, '10_leesified_3x_21.4096.jpg');

noise_variance = var(shirt(:))/3;
Otherwise = onez * noise_variance - local_var;
Otherwise(Otherwise>0) = 0;
Otherwise(Otherwise<0) = 1;

K = (1 - noise_variance./local_var).*Otherwise;
imshow_psnr('Leesified, noise variance / 3', K .* degraded + (onez-K).*local_mean, cameraman)
imwrite(K .* degraded + (onez-K).*local_mean, '_leesified_1_3.jpg');


noise_variance = var(shirt(:));
local_mean = colfilt(degraded, [10,10], 'sliding', @mean);
local_var = colfilt(degraded, [10,10], 'sliding', @var);
onez = ones(size(degraded,1), size(degraded,2));
Otherwise = onez * noise_variance - local_var;
Otherwise(Otherwise>0) = 0;
Otherwise(Otherwise<0) = 1;

K = (1 - noise_variance./local_var).*Otherwise;
imshow_psnr('Leesified, 2x Window', K .* degraded + (onez-K).*local_mean, cameraman)
imwrite(K .* degraded + (onez-K).*local_mean, '11_leesified_2xWindow_20.7047.jpg');



noise_variance = var(shirt(:));
local_mean = colfilt(degraded, [3,3], 'sliding', @mean);
local_var = colfilt(degraded, [3,3], 'sliding', @var);
onez = ones(size(degraded,1), size(degraded,2));
Otherwise = onez * noise_variance - local_var;
Otherwise(Otherwise>0) = 0;
Otherwise(Otherwise<0) = 1;

K = (1 - noise_variance./local_var).*Otherwise;
imshow_psnr('Leesified, 3x3 Window', K .* degraded + (onez-K).*local_mean, cameraman)
imwrite(K .* degraded + (onez-K).*local_mean, '_leesified_3x3Window.jpg');



