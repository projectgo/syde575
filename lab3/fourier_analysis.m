% Part 2: Fourier Analysis

clc; clear all; close all;

toy = zeros(256,256); toy(:,108:148)=1;

imfreq('Toy Image', toy);

toy_rotated = imrotate(toy, 45, 'nearest', 'crop');

imfreq('45 Degree Rotated Toy Image', toy_rotated);

lena = rgb2gray(imread('lena.tif'));

imfreq('Lena', lena);

lena_freq = fft2(lena, size(lena, 1), size(lena, 2));

lena_only_magnitude = ifft2(abs(lena_freq)/300);

figure;
imshow(lena_only_magnitude);
title('Lena, only Magnitude, 1/300th intensity');

lena_only_phase = ifft2(lena_freq ./ abs(lena_freq));

figure;
imshow(lena_only_phase, []);
title('Lena, only Phase');
