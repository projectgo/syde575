function freq = imshow_psnr(image_title, image)
   image_size = size(image);
   image_freq = fftshift(fft2(image, image_size(1), image_size(2)));
   image_freq_log = log(image_freq);

   figure;
   imshow(image);
   title(image_title);

   figure;
   imshow(image_freq, []);
   title(strcat(['Fourier Spectra of ', image_title]));

   figure;
   imshow(image_freq_log, []);
   title(strcat(['Log Fourier Spectra of ', image_title]));

   freq = image_freq;
end
