function imshow_save(image, name)
   figure;
   imshow(image);
   imwrite(image, strcat([ ...
      'img/', ...
      name, ...
      '.jpg', ...
   ]));
end
