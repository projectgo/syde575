% Part 3: Noise Reduction

clc; clear all; close all;

pure_lena = rgb2gray(imread('lena.tif'));
lena_size = size(pure_lena);
defiled_lena = imnoise(pure_lena, 'gaussian', 0, 0.005);

pure_lena_freq = imfreq('Lena', pure_lena);
defiled_lena_freq = imfreq('Gaussian Noised Lena', defiled_lena);

r = 60;
disk = fspecial('disk', r);
disk(disk > 0) = 1;
lowpass_filter_60 = zeros(512, 512);
lowpass_filter_60([512/2-r : 512/2+r], [512/2-r : 512/2+r]) = disk;

figure;
imshow(lowpass_filter_60);
title('Ideal Low-pass Filter, r = 60');

filter_60ed_lena = ifft2(ifftshift(defiled_lena_freq .* lowpass_filter_60));
imshow_psnr( ...
   'Ideal Low-pass Filtered Lena, r = 60', ...
   filter_60ed_lena, ...
   ifft2(ifftshift(pure_lena_freq)) ...
);

r = 20;
disk = fspecial('disk', r);
disk(disk > 0) = 1;
lowpass_filter_20 = zeros(512, 512);
lowpass_filter_20([512/2-r : 512/2+r], [512/2-r : 512/2+r]) = disk;

figure;
imshow(lowpass_filter_20);
title('Ideal Low-pass Filter, r = 20');

filter_20ed_lena = ifft2(ifftshift(defiled_lena_freq .* lowpass_filter_20));
imshow_psnr( ...
   'Ideal Low-pass Filtered Lena, r = 20', ...
   filter_20ed_lena, ...
   ifft2(ifftshift(pure_lena_freq)) ...
);

gaussian_filter= fspecial('gaussian', [512, 512], 60);
gaussian_filter = gaussian_filter ./ max(max(gaussian_filter));
gaussianed_lena = ifft2(ifftshift(defiled_lena_freq .* gaussian_filter));
figure;
imshow(gaussian_filter, []);
title('Gaussian Filter');

figure;
imshow_psnr( ...
   'Gaussian Low-pass Filtered Lena', ...
   gaussianed_lena, ...
   ifft2(ifftshift(pure_lena_freq)) ...
);


