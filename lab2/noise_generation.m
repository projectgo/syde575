clear; clc;
toy = [0.3*ones(200,100) 0.7*ones(200,100)];
gaussed_toy = imnoise(toy, 'gaussian', 0, 0.01);
peppered_toy = imnoise(toy, 'salt & pepper', 0.05);
specked_toy = imnoise(toy, 'speckle', 0.04);

close all;

title(imshow_psnr('Gaussian noised toy image', gaussed_toy, toy));
title(imshow_psnr( ...
   'Gaussian noised toy image', ...
   fft2(gaussed_toy, 200, 100), ...
   fft2(toy, 200, 100) ...
));
psnr(abs(fft2(gaussed_toy, 200, 100)), abs(fft2(toy, 200, 100)))
% title(imshow_psnr('Peppered toy image', peppered_toy, toy));
% title(imshow_psnr('Specked toy image', specked_toy, toy));
% 
% title(imhist_psnr('Gaussian noised toy image', gaussed_toy, toy));
% title(imhist_psnr('Peppered toy image', peppered_toy, toy));
% xlim([-0.01 1.01]);
% title(imhist_psnr('Specked toy image', specked_toy, toy));
