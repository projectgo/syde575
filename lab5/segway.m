function segmented = segway(image, centers, filename)
   image = applycform(image, makecform('srgb2lab'));

   image_l = image(:,:,1);
   image_a = image(:,:,2);
   image_b = image(:,:,3);

   ab = double(image(:,:,2:3));
   m = size(ab,1);
   n = size(ab,2);
   ab = reshape(ab, m*n, 2);
   [which_center, centers] = kmeans(ab, size(centers, 1), 'Start', centers);

   min_a = min(ab(:,1));
   min_b = min(ab(:,2));
   n_a = max(ab(:,1))-min_a+1;
   n_b = max(ab(:,2))-min_b+1;
   centers_a = (centers(:,1)-min_a);
   centers_b = (centers(:,2)-min_b);

   % figure;
   % plot(centers_b, centers_a, 'go', 'MarkerSize', 10, 'MarkerFaceColor', 'g');
   % hold on;
   % N = hist3(ab, [n_a n_b]);
   % contour(log(N)/log(20));
   % xlim([0 140]);
   % ylim([0 100]);
   % colorbar;

   ab = reshape(which_center, m, n);

   % imshow_save_scaled(ab, filename);
   segmented = ab;
end
