clear;
close all;

cameraman = im2double(imread('cameraman.tif'));
figure;
imshow(cameraman);
title('cameraman');

gaussian_filter = fspecial('gaussian', 7, 1);

gaussianed_cameraman = imfilter(cameraman, gaussian_filter);
figure;
imshow(gaussianed_cameraman);
title('Gaussianed cameraman');

figure;
imshow(cameraman - gaussianed_cameraman);
title('Cameraman - gaussianed cameraman');

figure;
imshow(cameraman + (cameraman - gaussianed_cameraman));
title('Cameraman + (cameraman - gaussianed cameraman)');

figure;
imshow(cameraman + 1.5 * (cameraman - gaussianed_cameraman));
title('Cameraman + 1.5 * (cameraman - gaussianed cameraman)');












