clc; clear all; close all;

og_peppers = imread('peppers.png');
peppers = rgb2ycbcr(imread('peppers.png'));

peppers_y  = peppers(:,:,1);
peppers_cb = peppers(:,:,2);
peppers_cr = peppers(:,:,3);

imshow_save(peppers_y, '1_peppers_y');
imshow_save(peppers_cb, '2_peppers_cb');
imshow_save(peppers_cr, '3_peppers_cr');

peppers_y_crap = imresize(imresize(peppers_y, 1/2), 2);
peppers_cb_crap = imresize(imresize(peppers_cb, 1/2), 2);
peppers_cr_crap = imresize(imresize(peppers_cr, 1/2), 2);

peppers_crap_chroma = peppers_y;
peppers_crap_chroma(:,:,2) = peppers_cb_crap;
peppers_crap_chroma(:,:,3) = peppers_cr_crap;
peppers_crap_chroma = ycbcr2rgb(peppers_crap_chroma);

imshow_save(peppers_crap_chroma, '4_peppers_crap_chroma');
imshow_save(og_peppers - peppers_crap_chroma, '5_peppers_crap_chroma_diff');

peppers_crap_luma = peppers_y_crap;
peppers_crap_luma(:,:,2) = peppers_cb;
peppers_crap_luma(:,:,3) = peppers_cr;
peppers_crap_luma = ycbcr2rgb(peppers_crap_luma);

imshow_save(peppers_crap_luma, '6_peppers_crap_luma');
imshow_save(og_peppers - peppers_crap_luma, '7_peppers_crap_luma_diff');
















